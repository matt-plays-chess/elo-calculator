function calculatePercentWinChance(playerRating, opponentRating) {
    return 1 / (1 + Math.pow(10, (opponentRating - playerRating) / 400));
}

function calculateChangeIfWin(playerRating, opponentRating) {
    var winProbability = calculatePercentWinChance(playerRating, opponentRating)
    return Math.round(32 * (1 - winProbability))
}

function calculateChangeIfLose(playerRating, opponentRating) {
    var winProbability = calculatePercentWinChance(opponentRating, playerRating)
    return Math.round(32 * (1 - winProbability))
}

function calculateNewRatingIfWin(playerRating, opponentRating){
    return playerRating + calculateChangeIfWin(playerRating, opponentRating);
}

function calculateNewRatingIfLose(playerRating, opponentRating){
    return playerRating - calculateChangeIfLose(playerRating, opponentRating);
}

function formatPercentage(rawPercentage) {
    return Math.round(rawPercentage * 100) + "%"
}